## Discord Scripts
A collection of console scripts for [discord](https://discord.com/)

## Discord System
Gives you the verified system tag

```js
var findModule = (item) => Object.values(webpackJsonp.push([[],{['']:(_,e,r)=>{e.cache=r.c}}, [['']]]).cache).find(m=>m.exports&&m.exports.default&&m.exports.default[item]!==void 0).exports.default;
findModule('getCurrentUser').getCurrentUser().system = true;
```
Credit: [Daddy Fweak](https://gitdab.com/fweak1337)

## Change system tag
Changes the system tag

```js
var findModule = (item) => Object.values(webpackJsonp.push([[],{['']:(_,e,r)=>{e.cache=r.c}}, [['']]]).cache).find(m=>m.exports&&m.exports.default&&m.exports.default[item]!==void 0).exports.default;

findModule('Messages').Messages.SYSTEM_DM_TAG_SYSTEM = 'System Tag Name Here';
```
Credit: [Daddy Fweak](https://gitdab.com/fweak1337)


## Get all Badges
Gives you most of the discord badges

```js
Object.values(webpackJsonp.push([[],{[''] :(_,e,r)=>{e.cache=r.c}},
[['']]]).cache).find(m=>m.exports&&m.exports.default&&m.exports.default.getCurrentUser!==void
0).exports.default.getCurrentUser().flags=-33
```

Credit: [Daddy Zebratic](https://github.com/Zebratic)

## Get Bot Tag
Gives you bot tag

```js
var findModule = (item) => Object.values(webpackJsonp.push([[],{['']:(_,e,r)=>{e.cache=r.c}}, [['']]]).cache).find(m=>m.exports&&m.exports.default&&m.exports.default[item]!==void 0).exports.default;
findModule('getCurrentUser').getCurrentUser().bot = true;
```

Credit: [con](https://www.google.com/)

## Get Token
Gives you the token of your account

```js
Object.values(webpackJsonp.push([[],{['']:(_,e,r)=>{e.cache=r.c}},[['']]]).cache).find(m=>m.exports&&m.exports.default&&m.exports.default.getToken!==void 0).exports.default.getToken()
```

Credit: [idk](https://pastebin.com/EG4YNbJD)


## Previews
<img src="https://belle.is-inside.me/NCO6YYhH.png"/>
<img src="https://belle.is-inside.me/7IVb4Vep.png"/>
<img src="https://belle.is-inside.me/O7PukdQi.png"/>
<img src="https://belle.is-inside.me/hoWnDE9O.png"/>
<img src="https://belle.is-inside.me/OEsmwoIv.png"/>

# Discord
Syz#0001 Send Nudes Pls